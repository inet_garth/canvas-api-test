# README #

This project was published as a companion to the following article:
* [.NET - Canvas API Implementation](https://community.canvaslms.com/groups/canvas-developers/blog/2016/11/05/net-canvas-api-implementation)

All code is provided as-is for demo purposes only.  This project was created as a demo, and is not considered production ready.  You will need to modify this code to meet your specific requirements.

### What is this repository for? ###

The purpose is to provide source code demonstrating how to leverage the Canvas API in a custom C# application.  The code can be easily modified to create many automated jobs or reports to assist with management of the Canvas environment.

### How do I get set up? ###

If you have a copy of Visual Studio, you should be able to download the solution, compile, and run.  Make sure you customize the myapi.config file to reflect your Canvas environment variables.